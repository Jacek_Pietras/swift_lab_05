//
//  StudentDetail.swift
//  Lab04
//
//  Created by Jacek on 15/03/2019.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class StudentDetail: UIViewController {
    
    var delegate:StudentDetailDelegate?
    var student: Student?

    @IBOutlet weak var firstNameInput: UITextField!
    @IBOutlet weak var lastNameInput: UITextField!
    @IBOutlet weak var indexNumberInput: UITextField!
    @IBOutlet weak var labNumberInput: UITextField!
    @IBOutlet weak var pointsInput: UITextField!
    @IBOutlet weak var button: UIButton!
    @IBAction func addStudentButton(_ sender: Any) {
        if student == nil {
            guard let name = self.firstNameInput.text else {
                print("Enter name")
                return
            }
            
            let newStudent = Student(firstName: name, lastName: self.lastNameInput.text!, indexNumber: self.indexNumberInput.text!, lab: Lab(number: Int(self.labNumberInput .text!)!, points: Double(self.pointsInput.text!)!))
            
            delegate?.createdNew(student: newStudent)
        } else {
            student?.firstName = self.firstNameInput.text!
            student?.lastName = self.lastNameInput.text!
            student?.indexNumber = self.indexNumberInput.text!
            student?.lab.number = Int(self.labNumberInput.text!)!
            student?.lab.points = Double(self.pointsInput.text!)!
            
            delegate?.editedOne(student: student!)
        }
        navigationController?.popViewController(animated:true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if student != nil {
            self.firstNameInput.text = student?.firstName
            self.lastNameInput.text = student?.lastName
            self.indexNumberInput.text = student?.indexNumber
            self.labNumberInput.text = String(student!.lab.number)
            self.pointsInput.text = String(student!.lab.points)
            self.button.setTitle("Update student", for: .normal)
        }
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
protocol StudentDetailDelegate {
    func createdNew(student:Student)
    func editedOne(student: Student)
}
